import java.util.Scanner;

class Records{
    float[] values;
    int[] keys;
    int c = 0;
    int v = 0;
    Records(int size){
        values = new float[size];
        keys = new int[size];
    }
    void setkey(float value){
        keys[c++] =  (int) value;
        if(values[c-1] != value) {
            if(v != c){
                v = c - 1;
                values[v++] = value;
            }
            else {
                values[--v] = value;
                v++;
            }
        }
    }
    void setvalue(){
        Scanner sc = new Scanner(System.in);
        float a  = sc.nextFloat(); 
        values[v++]= a;
        setkey(a);
    }
}