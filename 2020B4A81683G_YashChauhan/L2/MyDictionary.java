class MyDictionary {
  // Main Driver Method (optional in our modular design)
  public static void main(String args[]) {
    System.out.println("The best way to use this type is to run another app main and instantiate a dictionary in it.");
  }
  private int[] arr;
  private int size=0;
  private int count=0;
  public int Size() { return size; }
  public int Count() { return count; }

  MyDictionary(int initialSize) {
    arr=new int[initialSize];
    size=initialSize;
    count=0;
  }
  public void Insert(int elem) {
    if(size>count) {
      arr[count++]=elem;
    }
    else System.out.println("Size" + size + " not enough for holding an extra element after " + count + " count");
  }
  private static void swap(int[] arr, int i, int j)
  {
      int temp = arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
  }
  private static int partition(int[] arr, int low, int high)
  {
      // Choosing the pivot
      int pivot = arr[high];

      // Index of smaller element and indicates
      // the right position of pivot found so far
      int i = (low - 1);

      for (int j = low; j <= high - 1; j++) {

          // If current element is smaller than the pivot
          if (arr[j] < pivot) {

              // Increment index of smaller element
              i++;
              swap(arr, i, j);
          }
      }
      swap(arr, i + 1, high);
      return (i + 1);
  }
  private static void quickSort(int[] arr, int low, int high)
  {
      if (low < high) {

          // pi is partitioning index, arr[p]
          // is now at right place
          int pi = partition(arr, low, high);

          // Separately sort elements before
          // partition and after partition
          quickSort(arr, low, pi - 1);
          quickSort(arr, pi + 1, high);
      }
  }
  public void sort(){
    quickSort(arr,0, Count()-1);
  }
  public void replace(int element1,int element2){
    for(int i = 0 ; i < Size() ; i++){
      if(arr[i] == element1){
        arr[i] = element2;
        return;
      }
    }
    return;
  } 
  public int find(int element){
    for(int i = 0 ; i < Size() ; i++){
      if(arr[i] == element){
        return i;
      }
    }
    return -1;
  } 
  public void remove(int element){
    for(int i = 0 ; i < Size() ; i++){
      if(arr[i] == element){
        for(int j = i ; j < Size() ; j++){
          arr[j] = arr[j+1];
        }
        arr[--size] = 0;
        return ;
      }
    }
    return;
  }

  public void Show() {
    System.out.print("Printing Dictionary of " + size + " Size and " + count + " Occupancy:");
    for (int i = 0; i < count; i++) {
      System.out.print(" " + arr[i]);
    }
    for (int i = count; i < size; i++) {
      System.out.print(" .");
    }
    System.out.println("");
  }
}
